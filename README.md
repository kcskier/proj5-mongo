# Project 5: Brevet time calculator with API functionality

This is exactly like project 4, its a brevet calcaulator that uses Flask on the server, and Javascript and AJAX on the client.

Credit to UO CIS Department for the orignial program
New and updated functionality by Kendell Crocker kendellc@uoregon.edu

## What is in this repository

This is the source code for the program. Using your verisoning software of choice, download the contents of this repoistiory, and "run docker-compose up" (More detailed instructions below)
Note: Recommend using GIT, but other versioning software will work

## How to setup

You need docker. Below is a link to instructions on how to download and install docker for your machine:

https://docs.docker.com/install/

Also, you need docker-compose. Here's another link for that:

https://docs.docker.com/compose/install/

PLEASE NOTE THAT DOCKER ON WINDOWS DOES NOT ALWAYS FUNCTION AS EXPECTED. IT IS HIGHLY RECOMMENDED THAT YOU USE MAC OS OR A LINUX DISTRO OF YOUR CHOICE FOR THIS.
