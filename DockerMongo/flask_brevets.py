"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import *
import logging
import os

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

#Additonal Variables for mongoDB implementation
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client["brevetdb"]
collection = db["brevetdb"]

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")  
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############

#This is the normal entries
#User inputs a distance, and the server returns open and close times to the client
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    
    #Take these variables, its dangerous to go alone
    #This gets the distance between the control points
    km = int(request.args.get('km', 999, type=float))
    #This gets the distance of the brevet (Will only be 200, 300, 400, 600, 1000)
    distance = int(request.args.get('distance', 1000, type=float))
    #This gets the start date and time of the brevet
    datetime = str(request.args.get('start_datetime'))
    
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    #This will calculate and return the open and close time of the requested controle point
    open_time = acp_times.open_time(km, distance, datetime)
    close_time = acp_times.close_time(km, distance, datetime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#Submit
#This will submit the distances and times to a database (db)
@app.route("/_submit")
def _submit():
    
    #General Info
    distance = request.args.get('distance', 1000, type=float)
    start_datetime = str(request.args.get('start_datetime'))
    
    #here, have some variables from the client
    id_num = request.args.get('id', 1000, type=float)
    miles = request.args.get('miles', type=float)
    km = request.args.get('km', type=float)
    location = str(request.args.get('location'))
    control_open_time = str(request.args.get('open_time'))
    control_close_time = str(request.args.get('close_time'))
    
    if id_num == 100:
        try:
            app.logger.debug("try insert _id 100")
            collection.insert_one({'_id': id_num, 'distance': distance, 'start_datetime': start_datetime})
        except:
            collection.update_one({'_id': id_num},{'$set': {'_id': id_num, 'distance': distance, 'start_datetime': start_datetime}})
            app.logger.debug("try insert _id 100 failed, trying update")
    else:
        try:
            app.logger.debug("try insert normal entry")
            collection.insert_one({'_id': id_num, 'miles': miles, 'km': km, 'location': location, 'open': control_open_time, 'close': control_close_time})
        except:
            collection.update_one({'_id': id_num}, {'$set': {'_id': id_num, 'miles': miles, 'km': km, 'location': location, 'open': control_open_time, 'close': control_close_time}})
            app.logger.debug("try insert normal failed, trying update")
    return ""
    
#quick redirect for the display button
@app.route("/display")
def display():
    return flask.render_template('display.html')

#Display
#This will display the distances and times from the database (db)
@app.route("/_get_vals")
def _get_vals():

    #A single request from the client, but we return JSON elements to the client
    #to fill in the table
    id_num = request.args.get('_id', 1000, type=float)
    app.logger.debug("id_num is", id_num)
    
    if id_num == 100:
        app.logger.debug("id_num = 100 case has been called")
        result = collection.find_one({'_id': 100 })
    else:
        app.logger.debug("Id_num was not 100, else case has been called")
        result = collection.find_one({'_id': id_num})
    return flask.jsonify(result=result)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0", debug=True)
